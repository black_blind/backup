#!/bin/bash -e
TEMP=$( mktemp )
TEMP1=$( mktemp )
trap "rm -f '$TEMP'" EXIT
trap "rm -f '$TEMP1'" EXIT
whiptail --title  "Мастер полуавтоматической установки" --menu  "Выберите действие:" 15 60 2 \
1 "Установка" \
2 "Настройка" \
2>"$TEMP"
for action in $( cat "$TEMP" ); do
    case "$action" in
    '1')
	whiptail --title "Установка" --checklist "Выберите модули для установки:" 15 60 3 \
	1 'Базовые модули' on \
	2 'Модуль Yandex Disk'  off \
	3 'Вернуться назад' on \
	2>"$TEMP1"
	for selectact in $( cat "$TEMP1" ); do
	case "$selectact" in
	'"1"')	
	apt-get install davfs2 cifs-utils p7zip p7zip-full expect tree -yq
	chmod +x config-backup
	chmod +x backup-daily
	chmod +x backup-weekly
	cp backup-daily /usr/bin/
	cp backup-weekly /usr/bin/
	cp config-backup /usr/bin/
	;;
	'"2"')
	chmod +x yaru.exp
	echo 
	;;
	'"3"')
	sh install.sh
	;;
	esac
	done
        ;;
    '2')
	whiptail --title "Приветствие в конфигурации" --msgbox "Вас приветствует мастер по настройки резервного копирования. Нажмите Ок для продолжения..." 10 60
	config-backup
        ;;
    esac
done
